/*-------------------------------------------------------------------------------------
 * Template file for 4-pin I2C OLED display, e.g. from Geekcreit
 * using Adafruit SSD1306 driver and GFX libraries.
 * Tutorial:
 * https://startingelectronics.org/tutorials/arduino/modules/OLED-128x64-I2C-display/
 *-------------------------------------------------------------------------------------*/
#include <Wire.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_GFX.h>

// OLED display TWI address
#define OLED_ADDR   0x3C

// reset pin not used on 4-pin OLED module
Adafruit_SSD1306 display(-1);  // -1 = no reset pin


void setup() {
// initialize and clear display
  display.begin(SSD1306_SWITCHCAPVCC, OLED_ADDR);
  display.clearDisplay();
  display.display();

  // display a pixel in each corner of the screen
  display.drawPixel(0, 0, WHITE);
  display.drawPixel(127, 0, WHITE);
  display.drawPixel(0, 63, WHITE);
  display.drawPixel(127, 63, WHITE);

  // display a line of text
  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(27,30);
  display.print("Hello, world!");

  // update display with all of the above graphics
  display.display();
  
}

void loop() {
  // put your main code here, to run repeatedly:

}
